<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class StartController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        if ($this->getUser()){
            return $this->redirectToRoute('app_user_index');
        }
        return $this->render('AppBundle:Start:index.html.twig', array(

        ));
    }

}

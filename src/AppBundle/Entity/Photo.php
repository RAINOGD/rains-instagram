<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Photo
 *
 * @ORM\Table(name="photo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PhotoRepository")
 * @Vich\Uploadable
 */
class Photo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="photos", fileNameProperty="photoName")
     *
     * @var File
     */
    private $photo;

    /**
     * @var string
     * @ORM\Column(name="photo_name", type="text", nullable=true, unique=false)
     */
    private $photoName;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="owned_photos")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Commentary", mappedBy="photo")
     */
    private $commentaries;

    /**
     * @ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="liked_photos")
     */
    private $likers;

    public function __construct() {
        $this->likers = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set photo
     * @param File $photo
     * @return Photo
     */
    public function setPhoto(File $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return File|null
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set photoName
     *
     * @param string $photoName
     *
     * @return Photo
     */
    public function setPhotoName($photoName)
    {
        $this->photoName = $photoName;

        return $this;
    }

    /**
     * Get photoName
     *
     * @return string
     */
    public function getPhotoName()
    {
        return $this->photoName;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikers()
    {
        return $this->likers;
    }

    /**
     * Add liker
     *
     * @param \AppBundle\Entity\User $liker
     *
     * @return Photo
     */
    public function addLiker(\AppBundle\Entity\User $liker)
    {
        $this->likers[] = $liker;

        return $this;
    }

    /**
     * Remove liker
     *
     * @param \AppBundle\Entity\User $liker
     */
    public function removeLiker(\AppBundle\Entity\User $liker)
    {
        $this->likers->removeElement($liker);
    }

    /**
     * Add commentary
     *
     * @param \AppBundle\Entity\Commentary $commentary
     *
     * @return Photo
     */
    public function addCommentary(\AppBundle\Entity\Commentary $commentary)
    {
        $this->commentaries[] = $commentary;

        return $this;
    }

    /**
     * Remove commentary
     *
     * @param \AppBundle\Entity\Commentary $commentary
     */
    public function removeCommentary(\AppBundle\Entity\Commentary $commentary)
    {
        $this->commentaries->removeElement($commentary);
    }

    /**
     * Get commentaries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommentaries()
    {
        return $this->commentaries;
    }
}
